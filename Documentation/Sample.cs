﻿using System;
using AwesomeIoC;

namespace Documentation
{
    internal class Sample
    {
        public static void Main(string[] args)
        {
            //Требуется реализовать IoC Container, который можно будет использовать в любом .NET проекте

            //Он должен обладать следующими свойствами

            //1. Его можно создать и сконфигурировать. Смотри документацию на класс ContainerConfiguration

            var configuration = new ContainerConfiguration();

            //Так я передаю контейнеру информацию о том, что типы, которые контейнер должен распознавать
            //Находятся в сборке Documentation
            configuration.Assemblies(typeof(Sample).Assembly);

            var container = new Container(configuration);

            //2. Все объекты, которые разрешает контейнер должны быть синглтонами
            var a1 = container.Get<A>();
            var a2 = container.Get<A>();
            if (!ReferenceEquals(a1, a2))
                throw new Exception("Должен возвращаться один и тот же экземпляр");

            //3. Если во всех сборках, которые переданы контейнеру в качестве источника для распознавания, 
            //существует только единственная реализация интерфейса или абстрактного класса, 
            //то контейнер должен возвращать экземлпяр этой реализации без всякого допольнительного конфигурирования

            var a3 = container.Get<IA>();
            if (!ReferenceEquals(a2, a3))
                throw new Exception("Должен возвращаться один и тот же экземпляр");


            //4. Вложенные зависимости должны распознаваться 
            var b4 = container.Get<B4>();
            if (b4.SaySomethingAnother() !="Hello, world")
                throw new Exception("Внутренняя зависимость не распозналась");

            //5. Ну и просто ряд тестов
            var newConfiguration = new ContainerConfiguration()
                .Assemblies(typeof(Sample).Assembly)
                .Bind<IB, B2>()
                .Dependency<B3>("name", "Peter");

            var container2 = new Container(newConfiguration);
            var b = container2.Get<IB>();
            if (b.SaySomethingAnother() != "Hello, Bill")
                throw new Exception($"Должен был распознаться [B2], а пришло [{b.SaySomethingAnother()}]");
            
            var b3 = container2.Get<B3>();
            if (b3.SaySomethingAnother() != "Hello, Peter")
                throw new Exception("Неправильно распознался распознался параметр [name] для экземплера класса [B3]");
        }
    }

    public interface IA
    {
        string SaySomething();
    }

    public class A : IA
    {
        public string SaySomething()
        {
            return "Hello, world";
        }
    }

    public interface IB
    {
        string SaySomethingAnother();
    }

    public class B1 : IB
    {
        public string SaySomethingAnother()
        {
            return "Hello, Jack";
        }
    }

    public class B2 : IB
    {
        public string SaySomethingAnother()
        {
            return "Hello, Bill";
        }
    }

    public class B3 : IB
    {
        private readonly string name;

        public B3(string name)
        {
            this.name = name;
        }

        public string SaySomethingAnother()
        {
            return "Hello, " + name;
        }
    }

    public class B4 : IB
    {
        private readonly IA a;

        public B4(IA a)
        {
            this.a = a;
        }

        public string SaySomethingAnother()
        {
            return a.SaySomething();
        }
    }
}