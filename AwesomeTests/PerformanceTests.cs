using AwesomeIoC;
using AwesomeTests.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AwesomeTests
{
	[TestClass]
	public class PerformanceTests
	{
		private const int REPEAT = 1000000;

		[TestMethod]
		[Timeout(600)]
		[Description("������������ ��������� ������� �� ����������")]
		public void GetResolveInstanceTest()
		{
			var container = ContainerBuilder.GetSimpleContainer();

			for (int i = 0; i < REPEAT; i++)
				container.Get<SimpleImplementation>();
		}

		[TestMethod]
		[Timeout(1000)]
		[Description("������������ �������� ���������� � ��������� �� ���� �������")]
		public void GetInstanceFromNewContainerTest()
		{
			var configuration = new ContainerConfiguration();
			configuration.Assemblies(typeof (SimpleImplementation).Assembly);

			for (int i = 0; i < REPEAT; i++)
				new Container(configuration).Get<SimpleImplementation>();
		}

		[TestMethod]
		[Timeout(10000)]
		[Description("������������ ������������ ���������� � ��������� �� ���� �������")]
		public void GetInstanceWithConfigurationTest()
		{
			for (int i = 0; i < REPEAT; i++)
				ContainerBuilder.GetSimpleContainer().Get<SimpleImplementation>();
		}

		[TestMethod]
		[Timeout(15000)]
		[Description("������������ ������������ ���������� � ��������� �� ���� �������� �������")]
		public void GetDifficultInstanceWithConfigurationTest()
		{
			for (int i = 0; i < REPEAT; i++)
			{
				var configuration = new ContainerConfiguration()
					.Assemblies(typeof (SimpleImplementation).Assembly)
					.Bind<INotSoSimpleInterface, MostDifficultImplementation>()
					.Dependency<MostDifficultImplementation>("entity", "peace")
					.Dependency<MostDifficultImplementation>("action", "destroy");

				var container = new Container(configuration);

				container.Get<INotSoSimpleInterface>();
			}
		}
	}
}