﻿namespace AwesomeTests.Data
{
	public interface ISimpleInterface
	{
	}

	public abstract class SimpleAbstract
	{
	}

	public class SimpleImplementation : SimpleAbstract, ISimpleInterface
	{
	}

	public abstract class AnotherSimpleAbstract
	{
	}

	public class AnotherSimpleImplementation : AnotherSimpleAbstract
	{
	}

	public class InnerSimple
	{
		public readonly ISimpleInterface SimpleInterface;

		public InnerSimple(ISimpleInterface simpleInterface)
		{
			SimpleInterface = simpleInterface;
		}
	}
}