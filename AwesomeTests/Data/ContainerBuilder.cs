using AwesomeIoC;

namespace AwesomeTests.Data
{
	public static class ContainerBuilder
	{
		public static Container GetSimpleContainer()
		{
			var configuration = new ContainerConfiguration();
			configuration.Assemblies(typeof (SimpleImplementation).Assembly);

			return new Container(configuration);
		}
	}
}