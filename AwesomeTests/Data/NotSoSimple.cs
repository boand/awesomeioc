﻿using System;

namespace AwesomeTests.Data
{
	public interface INotSoSimpleInterface
	{
		void DoSomething();
	}

	public class NotSoSimpleImplementation : INotSoSimpleInterface
	{
		public void DoSomething()
			=> Console.WriteLine("thinking");
	}

	public class DifficultImplementation : INotSoSimpleInterface
	{
		public readonly string Entity;

		public DifficultImplementation(string entity)
		{
			Entity = entity;
		}

		public void DoSomething()
			=> Console.WriteLine($"pondering about {Entity}");
	}

	public class MoreDifficultImplementation : DifficultImplementation
	{
		public readonly string Action;

		public MoreDifficultImplementation(string entity, string action) : base(entity)
		{
			Action = action;
		}

		public new void DoSomething()
			=> Console.WriteLine($"pondering about {Entity} and {Action}");
	}

	public class MostDifficultImplementation : MoreDifficultImplementation
	{
		public readonly ISimpleInterface SimpleInterface;

		public MostDifficultImplementation(string entity, ISimpleInterface simpleInterface, string action) : base(entity, action)
		{
			SimpleInterface = simpleInterface;
		}
	}
}