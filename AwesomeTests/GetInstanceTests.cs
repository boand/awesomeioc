﻿using AwesomeIoC;
using AwesomeTests.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AwesomeTests
{
	[TestClass]
	public class GetInstanceTests
	{
		private Container _container;

		[TestInitialize]
		public void TestInitialize()
		{
			_container = ContainerBuilder.GetSimpleContainer();
		}

		[TestMethod]
		[Description("Можно получить объект по его классу")]
		public void GetImplementationInstanceTest()
		{
			var instance = _container.Get<SimpleImplementation>();

			Assert.IsNotNull(instance);
		}

		[TestMethod]
		[Description("Можно получить объект по интерфейсу")]
		public void GetInterfaceInstanceTest()
		{
			var instance = _container.Get<ISimpleInterface>();

			Assert.IsNotNull(instance);
			Assert.IsInstanceOfType(instance, typeof (SimpleImplementation));
		}

		[TestMethod]
		[Description("Можно получить объект по абстрактону классу")]
		public void GetAbstractInstanceTest()
		{
			var instance = _container.Get<AnotherSimpleAbstract>();

			Assert.IsNotNull(instance);
			Assert.IsInstanceOfType(instance, typeof (AnotherSimpleImplementation));
		}

		[TestMethod]
		[Description("Внутренняя зависимость")]
		public void ImplementationsEquals()
		{
			var instance = _container.Get<InnerSimple>();

			Assert.IsNotNull(instance.SimpleInterface);
			Assert.IsInstanceOfType(instance.SimpleInterface, typeof (SimpleImplementation));
		}
	}
}