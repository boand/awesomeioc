﻿using AwesomeIoC;
using AwesomeTests.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AwesomeTests
{
	[TestClass]
	public class SingletonTests
	{
		private Container _container;

		[TestInitialize]
		public void TestInitialize()
		{
			_container = ContainerBuilder.GetSimpleContainer();
		}

		[TestMethod]
		[Description("Объекты класса равны")]
		public void ImplementationsEquals()
		{
			var instance1 = _container.Get<SimpleImplementation>();
			var instance2 = _container.Get<SimpleImplementation>();
			Assert.AreEqual(instance1, instance2);
		}

		[TestMethod]
		[Description("Объекты класса и интерфейса равны")]
		public void ImplementationAndInterfaceEquals()
		{
			var instanceImplementation = _container.Get<SimpleImplementation>();
			var instanceInterface = _container.Get<ISimpleInterface>();
			Assert.AreEqual(instanceImplementation, instanceInterface);
		}

		[TestMethod]
		[Description("Объекты класса и абстрактного класса равны")]
		public void ImplementationAndAbstractEquals()
		{
			var instanceImplementation = _container.Get<AnotherSimpleImplementation>();
			var instanceAbstract = _container.Get<AnotherSimpleAbstract>();
			Assert.AreEqual(instanceImplementation, instanceAbstract);
		}

		[TestMethod]
		[Description("Объекты абстрактного класса и интерфейса равны")]
		public void InterfaceAndAbstractEquals()
		{
			var instanceAbstract = _container.Get<SimpleAbstract>();
			var instanceInterface = _container.Get<ISimpleInterface>();
			Assert.AreEqual(instanceAbstract, instanceInterface);
		}
	}
}