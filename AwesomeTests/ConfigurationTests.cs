﻿using AwesomeIoC;
using AwesomeTests.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AwesomeTests
{
	[TestClass]
	public class ConfigurationTests
	{
		[TestMethod]
		[Description("Можно получить объект без дополнительного конфигурирования")]
		public void SimpleGetInstanceTest()
		{
			var configuration = new ContainerConfiguration();
			configuration.Assemblies(typeof (SimpleImplementation).Assembly);
			var container = new Container(configuration);

			var instance = container.Get<SimpleImplementation>();

			Assert.IsNotNull(instance);
		}

		[TestMethod]
		[Description("Можно указать реализацию")]
		public void BindTest()
		{
			var configuration = new ContainerConfiguration()
				.Assemblies(typeof (SimpleImplementation).Assembly)
				.Bind<INotSoSimpleInterface, NotSoSimpleImplementation>();

			var container = new Container(configuration);
			var instance = container.Get<INotSoSimpleInterface>();

			Assert.IsNotNull(instance);
			Assert.IsInstanceOfType(instance, typeof (NotSoSimpleImplementation));
			instance.DoSomething();
		}

		[TestMethod]
		[Description("Можно заменить реализацию")]
		public void LastBindTest()
		{
			var configuration = new ContainerConfiguration()
				.Assemblies(typeof (SimpleImplementation).Assembly)
				.Bind<INotSoSimpleInterface, DifficultImplementation>()
				.Bind<INotSoSimpleInterface, NotSoSimpleImplementation>();

			var container = new Container(configuration);
			var instance = container.Get<INotSoSimpleInterface>();

			Assert.IsNotNull(instance);
			Assert.IsInstanceOfType(instance, typeof (NotSoSimpleImplementation));
			instance.DoSomething();
		}

		[TestMethod]
		[Description("Можно указать зависимости")]
		public void DependencyTest()
		{
			const string entity = "peace";

			var configuration = new ContainerConfiguration()
				.Assemblies(typeof (SimpleImplementation).Assembly)
				.Dependency<DifficultImplementation>("entity", entity);

			var container = new Container(configuration);
			var instance = container.Get<DifficultImplementation>();

			Assert.AreEqual(entity, instance.Entity);
			instance.DoSomething();
		}

		[TestMethod]
		[Description("Можно указать реализацию и зависимости")]
		public void BindDependencyTest()
		{
			var configuration = new ContainerConfiguration()
				.Assemblies(typeof (SimpleImplementation).Assembly)
				.Bind<INotSoSimpleInterface, DifficultImplementation>()
				.Dependency<DifficultImplementation>("entity", "peace");

			var container = new Container(configuration);
			var instance = container.Get<INotSoSimpleInterface>();

			Assert.IsNotNull(instance);
			Assert.IsInstanceOfType(instance, typeof (DifficultImplementation));
			instance.DoSomething();
		}

		[TestMethod]
		[Description("Можно указать несколько зависимостей")]
		public void ManyDependencyTest()
		{
			const string difficultEntity = "peace";
			const string moreDifficultEntity = "universe";
			const string moreDifficultAction = "create her";

			var configuration = new ContainerConfiguration()
				.Assemblies(typeof (SimpleImplementation).Assembly)
				.Dependency<DifficultImplementation>("entity", difficultEntity)
				.Dependency<MoreDifficultImplementation>("entity", moreDifficultEntity)
				.Dependency<MoreDifficultImplementation>("action", moreDifficultAction);

			var container = new Container(configuration);
			var difficultImplementation = container.Get<DifficultImplementation>();
			var moreDifficultImplementation = container.Get<MoreDifficultImplementation>();

			Assert.AreEqual(difficultEntity, difficultImplementation.Entity);
			difficultImplementation.DoSomething();

			Assert.AreEqual(moreDifficultEntity, moreDifficultImplementation.Entity);
			Assert.AreEqual(moreDifficultAction, moreDifficultImplementation.Action);
			moreDifficultImplementation.DoSomething();
		}
	}
}