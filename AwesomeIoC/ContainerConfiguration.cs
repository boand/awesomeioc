﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace AwesomeIoC
{
	/// <summary>Предоставляет набор методов для конфигурирования контейнера.</summary>
	public class ContainerConfiguration
	{
		private readonly ConcurrentDictionary<Type, Dictionary<string, object>> _dependencies = new ConcurrentDictionary<Type, Dictionary<string, object>>();
		private readonly Dictionary<Type, Type> _registeredTypes = new Dictionary<Type, Type>();

		/// <summary>Указывает контейнеру <see cref="AwesomeIoC.Container"/> список .NET сборок, типы из которых должны разрешаться</summary>
		/// <returns>Возвращает себя</returns>
		public ContainerConfiguration Assemblies(params Assembly[] assemblies)
		{
			var interfaces = new List<Type>();
			var types = new List<Type>();

			foreach (var type in assemblies.SelectMany(assembly => assembly.GetTypes()))
				if (type.IsInterface || type.IsAbstract)
					interfaces.Add(type);
				else
					types.Add(type);

			foreach (var @interface in interfaces)
			{
				var implementations = types.Where(type => @interface.IsAssignableFrom(type)).ToArray();
				if (implementations.Length == 1)
					Bind(@interface, implementations[0]);
			}
			return this;
		}

		/// <summary>Указывает контейнеру <see cref="AwesomeIoC.Container"/>, что в случае когда нужно разрешить экемпляр типа T, будет разрешаться экземпляр типа TImplementation</summary>
		/// <returns>Возвращает себя</returns>
		/// <example>
		/// <code>
		/// 
		/// public interface IA 
		/// {
		///     void DoSomething();
		/// }
		/// 
		/// public class A1: IA
		/// {
		///     public void DoSomething()
		///     {
		///         Console.WriteLine("sit down");
		///     }
		/// }
		/// 
		/// public class A2: IA
		/// {
		///     public void DoSomething()
		///     {
		///         Console.WriteLine("stand up");
		///     }
		/// }
		/// 
		/// var configuration = new ContainerConfiguration()
		///     .Assemblies(typeof(IA).Assembly)
		///     .Bind<IA, A2>();
		/// 
		/// var container = new Container(configuration);
		/// 
		/// var a = container.Get<IA>();
		/// 
		/// a.DoSomething(); // stand up
		/// 
		/// </code>
		/// </example>
		public ContainerConfiguration Bind<T, TImplementation>() where T : class where TImplementation : T
		{
			Bind(typeof (T), typeof (TImplementation));
			return this;
		}

		private void Bind(Type registeredType, Type implementationType)
		{
			if (_registeredTypes.ContainsKey(registeredType))
				_registeredTypes[registeredType] = implementationType;
			else
				_registeredTypes.Add(registeredType, implementationType);
		}

		/// <summary>Указывает контейнеру <see cref="AwesomeIoC.Container"/> значение, которое должно быть использовано в качестве зависимости</summary>
		/// <param name="parameterName">Имя параметра конструктора типа TImplementation</param>
		/// <param name="parameterValue">Значение зависимости, которое должно быть использовано при конструировании объекта типа TImplementation</param>
		/// <returns>Возвращает себя</returns>
		/// <example>
		/// <code>
		/// 
		/// public class A
		/// {
		///     private readonly string value;
		/// 
		///     public A(string value)
		///     {
		///         this.value = value;
		///     }
		/// 
		///     public void DoSomething() 
		///     {
		///         Console.WriteLine(value);
		///     }
		/// }
		/// 
		/// var configuration = new ContainerConfiguration()
		///     .Assemblies(typeof(A).Assembly)
		///     .Dependency<A>("value", "Hello, world");
		/// 
		/// var container = new Container(configuration);
		/// 
		/// var a = container.Get<A>();
		/// 
		/// a.DoSomething(); // Hello, world
		/// 
		/// </code>
		/// </example>
		public ContainerConfiguration Dependency<TImplementation>(string parameterName, object parameterValue)
		{
			var dependency = _dependencies.GetOrAdd(typeof (TImplementation), new Dictionary<string, object>());
			dependency.Add(parameterName, parameterValue);
			return this;
		}

		internal Binding GetBinding(Type type)
		{
			var resolvedType = _registeredTypes.ContainsKey(type) ? _registeredTypes[type] : type;
			Dictionary<string, object> dependencies;
			_dependencies.TryGetValue(resolvedType, out dependencies);

			return new Binding(resolvedType, dependencies);
		}
	}
}