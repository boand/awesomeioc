﻿using System;
using System.Collections.Generic;

namespace AwesomeIoC
{
	internal class Binding
	{
		private readonly Dictionary<string, object> _dependencies;

		internal Binding(Type resolvedType, Dictionary<string, object> dependencies)
		{
			ResolvedType = resolvedType;
			_dependencies = dependencies;
		}

		internal Type ResolvedType { get; private set; }

		internal bool ExistsDependency(string name)
			=> _dependencies != null && _dependencies.ContainsKey(name);

		internal object GetDependency(string name)
			=> _dependencies[name];
	}
}