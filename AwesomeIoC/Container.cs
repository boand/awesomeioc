﻿using System;
using System.Collections.Concurrent;
using System.Linq;

namespace AwesomeIoC
{
	public class Container
	{
		private readonly ContainerConfiguration _configuration;
		private readonly ConcurrentDictionary<Type, object> _instances = new ConcurrentDictionary<Type, object>();

		public Container(ContainerConfiguration configuration)
		{
			_configuration = configuration;
		}

		/// <summary>
		/// Создаёт или возвращает экземпляр сервиса который является объектом типа T,
		/// либо наследником абстрактного класса N либо реализует интерфейс T,
		/// </summary>
		/// <typeparam name="T">Тип конкретного класса, абстрактного класса и интерфейса</typeparam>
		/// <returns>Экземпляр объекта, является синглтоном</returns>
		public T Get<T>() where T : class
		{
			T instance = (T) Get(typeof (T));
			return instance;
		}

		protected object Get(Type type)
		{
			try
			{
				var binding = _configuration.GetBinding(type);
				return _instances.GetOrAdd(binding.ResolvedType, CreateInstance(binding));
			}
			catch (Exception ex)
			{
				throw new Exception($"Невозможно получить тип '{type.FullName}'", ex);
			}
		}

		/// <summary>Создание экземпляра</summary>
		private object CreateInstance(Binding binding)
		{
			//получаем конструктор
			var constructor = binding.ResolvedType.GetConstructors().FirstOrDefault();

			if (constructor == null)
				throw new Exception($"Отсутсвует публичный конструктор для '{binding.ResolvedType.FullName}'");

			var parameterInfos = constructor.GetParameters();

			if (!parameterInfos.Any())
				//если конструктор без параметров, то создаем экземляр
				return Activator.CreateInstance(binding.ResolvedType);

			var constructorParams = parameterInfos.Select(param =>
				binding.ExistsDependency(param.Name)
					? binding.GetDependency(param.Name)
					: Get(param.ParameterType));

			var instance = constructor.Invoke(constructorParams.ToArray());
			return instance;
		}
	}
}